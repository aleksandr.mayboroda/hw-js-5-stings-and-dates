
function startTask() {
    
    let user = createNewUser()

    if(user)
    {
        console.log(user)
        console.log('login is:',user.getLogin())
        console.log('password is:',user.getPassword())
       
        // user.setFirstName('new name')
        // user.setLastName('new lastName')
        console.log('birth date is:',user.birthDate)
        console.log('age is:',user.getAge())
        
        console.log(user)
    }
    else
    {
        console.error('smth gone wrong!')
    }
  

}
function createNewUser()
{
    let firstName = prompt('Введи Имя'),
        lastName = prompt('Введи Фамилию'),
        birthDate = prompt('Введи дату рождения в формате dd.mm.yyyy')

    while(!isNaN(firstName) || !isNaN(lastName))
    {
        firstName = prompt('Введи Имя снова',firstName),
        lastName = prompt('Введи Фамилию снова',lastName)
    }

    let birthDateCheck = pastDateCheck(birthDate)

    while(birthDateCheck == false)
    {
        birthDate = prompt('Введи дату рождения в ПРАВИЛЬНОМ формате dd.mm.yyyy',birthDate)
        birthDateCheck = pastDateCheck(birthDate)
    }
    birthDate = birthDateCheck //в самом объекте не примет  

    const user = {
        firstName,
        lastName,
        birthDate, //если есть setter, то передача значения здесь игнорируется
        get birthDate() //getter
        {
            let date = new Date(birthDate),
                day,
                month
            day = (date.getDate() < 10) ? '0' + date.getDate() : date.getDate()
            month = (date.getMonth() + 1 < 10) ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
            return `${day}.${month}.${date.getFullYear()}`
        },
        set birthDate(newBirtDate) //setter
        {
            let newBirthCheck = pastDateCheck(newBirtDate)
            if(newBirthCheck)
            {
                birthDate = newBirthCheck
            }
        },
        getAge()
        {
            let date = new Date(birthDate), //без this, чтобы не сработал getter - вернул временную метку timestamp
                today = new Date(),
                diff = 0
            
            if(date.getTime() < today.getTime())
            {
                diff = today.getFullYear() - date.getFullYear()

                // если текущий месяцы равны и сегодняшняя дата < дата рождения
                // или текущий месяц меньше месяца рождения
                if(
                    (today.getDate() < date.getDate() && today.getMonth() == date.getMonth())
                    ||
                    (today.getMonth() < date.getMonth())
                    )
                {
                    --diff  
                }
            }
            
            return diff
        },
        getPassword: function()
        {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDate.slice(-4)
        },
        getLogin: function()
        {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        setFirstName(newName) //additional task
        {
            console.log('Меняем имя на ',newName)
            if(newName)
            {
                this.setPropValue('firstName',newName) 
            }
           
        },
        setLastName(newLastName)  //additional task
        {
            console.log('Меняем фамилию на ',newLastName)
            if(newLastName)
            {
                this.setPropValue('lastName',newLastName) 
            }
        },
        setPropValue(propName,propValue)  //additional task
        {
            Object.defineProperty(
                this,
                propName,
                {
                    value: propValue,
                },   
            )
        }
    }

    //additional task

    Object.defineProperties(
        user,
        {
            'firstName': {
                writable: false
            },   
            'lastName': {
                writable: false
            },  
        }      
    )

    return user
}

 //служебная функция проверки даты, чтобы дата и не больше текущей
function pastDateCheck(date)
{
    let dateArr = date.split('.'),
        result

    result = new Date(`${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`) 

    return (result == 'Invalid Date' || result.getTime() >= new Date().getTime()) 
        ? false
        : result.getTime()
}